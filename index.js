const express = require("express");
const dotenv = require("dotenv").config();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const UserEntry = require("./models/user-entry");
const Log = require("./models/log-entry");
const BannedIps = require("./models/banned-ips");
const SafeIps = require("./models/safe-ips");
const Config = require("./models/config");
const moment = require("moment-timezone");
const { config } = require("dotenv");

const TESTING = false;

const app = express();
app.use(cors());

const PORT = process.env.PORT || 5000;

const weekdayToString = new Map([
  [0, "Shouldn't be able to get 0"],
  [1, "Monday"],
  [2, "Tuesday"],
  [3, "Wednesday"],
  [4, "Thursday"],
  [5, "Friday"],
  [6, "Saturday"],
  [7, "Sunday"],
]);

const getCphTime = subtract_days => {
  const cph_time = moment()
    .tz("Europe/Copenhagen")
    .subtract(subtract_days || 0, "days")
    .format();
  const regex = /(20\d\d)-(\d\d)-(\d\d)T(\d\d:\d\d:\d\d).*/g;
  const matches = Array.from(cph_time.matchAll(regex));
  const [full, year, month, date, time] = matches[0];
  return {
    timestamp: full,
    weekday: moment(`${year}-${month}-${date}`).isoWeekday(),
    currentDateString: `${date}.${month}.${year}`,
    currentDate: { year, month, date },
    momentdate: moment(`${year}-${month}-${date}`),
    currentTime: time,
  };
};

const getLastWeekday = start => {
  let { weekday, momentdate } = getCphTime();
  if (start) {
    const regex = /(\d\d).(\d\d).(\d\d\d\d)/g;
    const matches = Array.from(start.matchAll(regex));
    const [full, date, month, year] = matches[0];
    start = `${year}-${month}-${date}`;
    console.log(start);
    weekday = moment(start).isoWeekday();
    console.log(weekday);
  }
  let subtractedDays = weekday === 1 ? 3 : weekday === 7 ? 2 : 1;
  console.log(subtractedDays);
  if (start) {
    subtractedDays = subtractedDays + momentdate.diff(moment(start), "days");
    console.log(subtractedDays);
  }
  return getCphTime(subtractedDays).currentDateString;
};

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGODB_PASSWORD}@${process.env.MONGODB_CLUSTER}?retryWrites=true&w=majority`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => {
    app.use(bodyParser.json());

    app.get("/leaderboard", async (req, res, next) => {
      const {
        weekday,
        currentTime: time,
        currentDateString: date,
      } = getCphTime();

      const config_continueStreak = await Config.findOne({
        key: "continueStreak",
      });
      const yesterday = getLastWeekday(
        config_continueStreak.value === "true"
          ? config_continueStreak.date
          : undefined
      );

      try {
        const allEntries = await UserEntry.find();
        const valid_entries = allEntries.filter(
          e =>
            e.frozen ||
            e.last_entry === (weekday > 5 ? yesterday : date) ||
            (config_continueStreak.value === "true" &&
              e.last_entry === yesterday) ||
            (time < "09:05:00" && e.last_entry === yesterday)
        );
        res.send(valid_entries);
      } catch (err) {
        console.log(err);
      }
    });

    app.get("/submit/", async (req, res, next) => {
      const ip = req.query.ip;
      const username = req.query.username;
      const {
        timestamp,
        weekday,
        currentTime: time,
        currentDateString: date,
        currentDate,
      } = getCphTime();

      const shouldFreeze =
        (Number(currentDate.month) === 12 && Number(currentDate.date) > 20) ||
        (Number(currentDate.month) === 1 && Number(currentDate.date) < 3);

      const banned = await BannedIps.findOne({ ip });
      if (banned) {
        res.status(401).send({
          message:
            "Sorry, you are not allowed to submit. Contact hosts of the roll call for more information.",
        });
        const log = new Log({
          username,
          timestamp,
          weekday: weekdayToString.get(weekday),
          description: "Fail: Banned IP.",
          ip,
        });
        await log.save();
        return;
      }

      if (!TESTING && (weekday == 6 || weekday == 7)) {
        res.status(400).send({
          message: "No roll call in the weekends.",
        });
        const log = new Log({
          username,
          timestamp,
          weekday: weekdayToString.get(weekday),
          description: "Fail: Submit on the weekend.",
          ip,
        });
        await log.save();
        return;
      }

      try {
        if (TESTING || (time < "09:05:00" && time > "05:55:00")) {
          // valid timeframe
          const logs = await Log.findOne({
            ip,
            timestamp: { $gte: timestamp.slice(0, 10) },
            description: { $regex: "Success" },
          });
          if (!TESTING && logs) {
            // this IP already submitted today
            const safeIp = await SafeIps.findOne({ ip });
            if (!safeIp) {
              res.status(400).send({
                message: "You can only submit once a day.",
              });
              const log = new Log({
                username,
                timestamp,
                weekday: weekdayToString.get(weekday),
                description: `Fail: Submit from repeat IP - IP's last entry was with username ${username}.`,
                ip,
              });
              await log.save();
              return;
            }
          }
          const existing = await UserEntry.findOne({
            username,
          });
          if (existing === null) {
            // first entry
            const entry = new UserEntry({
              username,
              last_entry: date,
              streak: 1,
              frozen: shouldFreeze,
            });
            await entry.save();
            res.status(201).send({
              username,
              last_entry: date,
              streak: 1,
              frozen: shouldFreeze,
            });

            const log = new Log({
              username,
              timestamp,
              weekday: weekdayToString.get(weekday),
              description:
                "Success: New user, created new database entry with streak 1.",
              ip,
            });
            await log.save();
          } else if (existing.last_entry === date) {
            // username already submitted today
            res.status(409).send({
              message: "This user has already been submitted today.",
            });
            const log = new Log({
              username,
              timestamp,
              weekday: weekdayToString.get(weekday),
              description: `Fail: Submit from repeat user - user's last entry was ${existing.last_entry}.`,
              ip,
            });
            await log.save();
          } else {
            // not first entry, but valid
            const config_continueStreak = await Config.findOne({
              key: "continueStreak",
            });
            const yesterday = getLastWeekday();

            const continueStreak =
              existing.last_entry === yesterday ||
              config_continueStreak.value === "true";
            const streak =
              continueStreak || existing.frozen ? existing.streak + 1 : 1;

            const filter = { username };
            const options = { upsert: false };
            const update = {
              $set: {
                streak: streak,
                last_entry: date,
                frozen: shouldFreeze,
              },
            };
            await UserEntry.updateOne(filter, update, options);

            res.status(201).send({
              username,
              last_entry: date,
              streak: streak,
              frozen: shouldFreeze,
            });

            const log = new Log({
              username,
              timestamp,
              weekday: weekdayToString.get(weekday),
              description: existing.frozen
                ? `Success: Frozen participant - updated streak from ${existing.streak} to ${streak}.`
                : continueStreak
                ? `Success: Last entry was ${existing.last_entry} - Updated streak from ${existing.streak} to ${streak}.`
                : `Success: Last entry was ${existing.last_entry}, but should have been on ${yesterday} to continue streak - Reset streak from ${existing.streak} to ${streak}.`,
              ip,
            });
            await log.save();
          }
        } else {
          // invalid timeframe
          res.status(400).send({
            message: "You can only submit between 5:55 and 8:05 CET.",
          });
          const log = new Log({
            username,
            timestamp,
            weekday: weekdayToString.get(weekday),
            description: "Fail: Invalid timeframe - before 5:55 or after 8:05.",
            ip,
          });
          await log.save();
        }
      } catch (err) {
        console.log(err);
      }
    });

    app.get("/all-logs", async (req, res, next) => {
      var date = req.query.date;
      var username = req.query.username;
      var ip = req.query.ip;
      var page = req.query.page;
      if (!page) page = 1;
      if (!date && !username && !ip) date = getCphTime().timestamp.slice(0, 10);
      try {
        var allLogs = await Log.find({
          timestamp: { $regex: date || "", $options: "i" },
          username: { $regex: username || "", $options: "i" },
          ip: { $regex: ip || "", $options: "i" },
        });
        allLogs = allLogs.sort((a, b) => {
          return a.timestamp.localeCompare(b.timestamp);
        });
        res.send({
          totalCount: allLogs.length,
          logs: allLogs.slice((page - 1) * 20, page * 20),
        });
      } catch (err) {
        console.log(err);
      }
    });

    app.get("/admin-revert", async (req, res, next) => {
      try {
        const requestIp = req.query.requestIp;
        const revertIp = req.query.revertIp;
        const date = req.query.date;
        const safeip = await SafeIps.findOne({
          ip: requestIp,
        });
        if (safeip) {
          const logs = await Log.find({
            ip: revertIp,
            timestamp: {
              $regex: date.slice(0, 10),
            },
          });
          res.status(200).send({ count: logs.length, success: true });
        } else {
          res.status(401).send({ success: false });
        }
      } catch (err) {
        console.log(err);
      }
    });

    app.get("/admin-revert-confirm", async (req, res, next) => {
      try {
        const requestIp = req.query.requestIp;
        const revertIp = req.query.revertIp;
        const date = req.query.date;
        const safeip = await SafeIps.findOne({
          ip: requestIp,
        });
        if (safeip) {
          const logs = await Log.find({
            ip: revertIp,
            timestamp: {
              $regex: date.slice(0, 10),
            },
          });
          logs.forEach(async log => {
            if (log.description.includes("Reverted")) return;
            if (log.description.includes("Success")) {
              const other = await Log.findOne({
                username: log.username,
                timestamp: {
                  $gte: date.slice(0, 10),
                  $lte: date.slice(0, 10) + "T23:59:59",
                },
                ip: { $ne: revertIp },
              });
              if (!other) {
                if (log.description.includes("New user")) {
                  await UserEntry.deleteOne({ username: log.username });
                } else {
                  const regex1 = /from (\d+) to \d+/g;
                  const [full1, oldStreak] = regex1.exec(log.description);
                  const regex2 = /Last entry was (\d\d.\d\d.\d\d\d\d)/g;
                  const match = regex2.exec(log.description);
                  const filter = { username: log.username };
                  const options = { upsert: false };
                  const update = {
                    $set: {
                      streak: parseInt(oldStreak),
                      last_entry: match ? match[1] : date,
                      frozen: log.description.includes("Frozen"),
                    },
                  };
                  await UserEntry.updateOne(filter, update, options);
                }
              }
            }
            const newDescription = `Reverted: ${log.description
              .split(":")
              .splice(1)
              .join(":")}`;
            await Log.updateOne(
              { _id: log._id },
              { $set: { description: newDescription } },
              { upsert: false }
            );
          });
          res.status(200).send({ success: true });
        } else {
          res.status(401).send({ success: false });
        }
      } catch (err) {
        console.log(err);
      }
    });

    app.get("/admin-login", async (req, res, next) => {
      try {
        const ip = await SafeIps.findOne({
          ip: req.query.ip,
        });
        if (ip) {
          res.status(200).send({ success: true });
        } else {
          res.status(401).send({ success: false });
        }
      } catch (err) {
        console.log(err);
      }
    });

    app.get("/frozen", async (req, res, next) => {
      try {
        const frozen = await UserEntry.find({ frozen: true });
        const usernames = frozen.map(e => e.username);
        res.send({ usernames });
      } catch (err) {
        console.log(err);
      }
    });

    app.get("/admin-freeze", async (req, res, next) => {
      try {
        const requestIp = req.query.requestIp;
        const username = req.query.username;
        const frozen = req.query.frozen === "true";
        const safeip = await SafeIps.findOne({
          ip: requestIp,
        });
        if (safeip) {
          const filter = {
            username: { $regex: `^${username}$`, $options: "i" },
          };
          const options = { upsert: false };
          const update = {
            $set: {
              frozen,
            },
          };
          await UserEntry.updateOne(filter, update, options);
          res.status(201).send({
            username,
            frozen,
          });
        } else {
          res.status(401).send({ success: false });
        }
      } catch (err) {
        console.log(err);
      }
    });

    app.get("/admin-config", async (req, res, next) => {
      try {
        const requestIp = req.query.requestIp;
        const safeip = await SafeIps.findOne({
          ip: requestIp,
        });
        if (safeip) {
          const config = await Config.find();
          res.status(200).send(config);
        } else {
          res.status(401).send({ success: false });
        }
      } catch (err) {
        console.log(err);
      }
    });
    app.put("/admin-config", async (req, res, next) => {
      try {
        const requestIp = req.query.requestIp;
        const safeip = await SafeIps.findOne({
          ip: requestIp,
        });
        if (safeip) {
          const { currentDateString: date } = getCphTime();

          const key = req.query.key;
          const value = req.query.value;
          const filter = { key };
          const options = { upsert: false };
          const update = { $set: { value, date } };
          await Config.updateOne(filter, update, options);
          res.status(201).send({
            key,
            value,
          });
        } else {
          res.status(401).send({ success: false });
        }
      } catch (err) {
        console.log(err);
      }
    });

    app.listen(PORT, () => {
      console.log(`Server is running on ${PORT}`);
    });
  })
  .catch(err => {
    console.log(err);
  });
