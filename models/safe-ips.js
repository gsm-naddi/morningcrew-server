const mongoose = require("mongoose");

const safeIPschema = new mongoose.Schema({
    ip: { type: String, required: true },
    description: { type: String, required: false },
});

module.exports = mongoose.model("safeIps", safeIPschema);
