const mongoose = require("mongoose");

const userEntrySchema = new mongoose.Schema({
    username: { type: String, required: true },
    last_entry: { type: String, required: true },
    streak: { type: Number, required: true },
    frozen: { type: Boolean, required: true },
});

module.exports = mongoose.model("userSettings", userEntrySchema);
