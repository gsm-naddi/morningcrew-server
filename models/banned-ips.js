const mongoose = require("mongoose");

const bannedIpSchema = new mongoose.Schema({
    ip: { type: String, required: true },
    description: { type: String, required: false },
});

module.exports = mongoose.model("bannedIps", bannedIpSchema);
