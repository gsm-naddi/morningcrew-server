const mongoose = require("mongoose");

const configSchema = new mongoose.Schema({
  key: { type: String, required: true },
  value: { type: String, required: true },
  date: { type: String, required: true },
});

module.exports = mongoose.model("config", configSchema);
