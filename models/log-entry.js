const mongoose = require("mongoose");

const logSchema = new mongoose.Schema({
    username: { type: String, required: true },
    timestamp: { type: String, required: true },
    weekday: { type: String, required: true },
    description: { type: String, required: true },
    ip: { type: String, required: false },
});

module.exports = mongoose.model("logs", logSchema);
